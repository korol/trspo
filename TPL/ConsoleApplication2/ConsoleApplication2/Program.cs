﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;

namespace QuickSort
{
    class Program
    {
        static void q_sort(int[] array, int left, int right, bool forceSequential)
        {

            int current, last;
            if (left >= right) return;
            swap(array, left, (left + right) / 2);
            last = left;
            for (current = left + 1; current <= right; ++current)
            {
                if (array[current] < array[left])
                {
                    ++last;
                    swap(array, last, current);
                }
            }
            swap(array, left, last);

            if (forceSequential)
            {
                q_sort(array, left, last - 1, forceSequential);
                q_sort(array, last + 1, right, forceSequential);               
            }
            else
            {
                Task task1 = new Task(() => q_sort(array, left, last - 1, forceSequential));
                 task1.Start();
                 Console.ForegroundColor = ConsoleColor.Green;
                 Console.Write(task1);
                 Task task2 = new Task(() => q_sort(array, last + 1, right, forceSequential));
                task2.Start();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(task2);
            }
        }
        static void swap(int[] array, int i, int j)
        {
            int temp;
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        static void InitArray(int totalLength, int[] array)
        {
            for (int i = 0; i < array.Length; ++i) array[i] = totalLength--;
        }
        static void Main(string[] args)
        {
            for (int iters = 0; iters < 10; iters++)
            {
               int[] array;
                int totalLength;
           
                // Parallel
                array = new int[100];
                totalLength = array.Length;
                InitArray(array.Length, array);                
                q_sort(array, 0, array.Length - 1, false);                            
                Console.ReadKey();
            }
        }
    }
}