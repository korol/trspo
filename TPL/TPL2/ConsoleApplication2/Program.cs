﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Threading.Tasks;
namespace QuickSort
{
    class Program
    {
        static void InitArray(int totalLength, int[] array)
        {
            for (int i = 0; i < array.Length; ++i) array[i] = totalLength--;
        }

        static void q_sort(int[] array, int left, int right, bool forceSequential)
        {
            int current, last;
            if (left >= right) return;
            swap(array, left, (left + right) / 2);
            last = left;
            for (current = left + 1; current <= right; ++current)
            {
                if (array[current] < array[left])
                {
                    ++last;
                    swap(array, last, current);
                }
            }
            swap(array, left, last);

            if (forceSequential || (last - left) < 4096)
            {
                q_sort(array, left, last - 1, forceSequential);
                q_sort(array, last + 1, right, forceSequential);
            }
            else
            {
                Parallel.Invoke(
  delegate { q_sort(array, left, last - 1, forceSequential); },
    delegate { q_sort(array, last + 1, right, forceSequential); });
            }
        }

        static void swap(int[] array, int i, int j)
        {
            int temp;
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        static void Main(string[] args)
        {
            for (int iters = 0; iters < 10; iters++)
            {
                int[] array;
                int totalLength;
                Stopwatch watch;
                // Sequential
                array = new int[1000000];
                InitArray(array.Length, array);
                watch = Stopwatch.StartNew();
                q_sort(array, 0, array.Length - 1, true);
                watch.Stop();
                Console.WriteLine("Sequential: {0} s", watch.Elapsed);
                // Parallel
                array = new int[1000000];
                totalLength = array.Length;
                for (int i = 0; i < array.Length; ++i) array[i] = totalLength--;
                watch = Stopwatch.StartNew();
                q_sort(array, 0, array.Length - 1, false);
                watch.Stop();
                Console.WriteLine("Parallel: {0} s", watch.Elapsed);
                Console.ReadKey();
            }
        }
    }
}
