// TRSPO2.cpp : Defines the entry point for the console application.
//

	#include "stdafx.h"
	#include <omp.h>
	#include <iostream>
	#include <Windows.h>
    #include <time.h>
	static class MultiplyingResult {
		public:
				static void SerialMultiplying(int arr[], int n){
						int serialCount = 1;
						for (int i = 0; i < n; i++){
								serialCount = serialCount*arr[i];
				
			};
				std::cout << "SerialResult " << serialCount << std::endl;
			
		}
		
				static void ParallelMultiplying(int arr[], int n){
					int count = 1;
				#pragma omp parallel shared(arr, count, n)
							{
				#pragma omp for
								for (int i = 0; i < n; i++)
								count = count*arr[i];
							}
				std::cout << "ParallelResult " << count << std::endl;
			
		}
		
};
	int main(int argc, char* argv[])
	{
			int A[10], i, count, n = 30;
			/* �������� �������� ������� */
				A[0] = 3;
		for (i = 1; i < n; i++){
					A[i] = i;
		
	}
      		double sStart = omp_get_wtime();
			MultiplyingResult::SerialMultiplying(A, n);
		    std::cout << "Time of calculation " << omp_get_wtime() - sStart << " seconds" << std::endl;
			double pStart = omp_get_wtime();
		    MultiplyingResult::ParallelMultiplying(A, n);
			std::cout << "Time of calculation " << omp_get_wtime() - pStart << " seconds" << std::endl;
			system("PAUSE");
			return 0;
		}

