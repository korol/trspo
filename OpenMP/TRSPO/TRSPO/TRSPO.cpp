// TRSPO.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <clocale>
#include <omp.h>
using namespace std;

class MyArray{
public:
	virtual void InitArray(int array[][100], int n)
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				array[i][j] = rand() % 100 + 3;
	}

	virtual void ShowArray(int arr[][100]){
#pragma omp parallel 
#pragma omp for ordered
		for (int i = 0; i < 100; i++)
		{
#pragma omp ordered
			for (int j = 0; j < 100; j++){
				std::cout << arr[i][j] << " ";
			}
			std::cout << "\n";
		}
	}

	void ToOneDimentionalArray(int array[][100], int a[], int n)
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
			{
			a[n*i + j] = array[i][j];
			}
	}

	void ToMultiDimentionalArray(int array[][100], int a[], int n)
	{
		for (size_t i = 0, k = 0; i < n; ++i)
			for (size_t j = 0; j < n; ++j)
				array[i][j] = a[k++];
	}

public: virtual void sortArray(int* a, const long n) {
	long i = 0, j = n;
	float pivot = a[n / 2];
#pragma omp parallel 
	{
#pragma omp single
	{
		do {
			while (a[i] < pivot) i++;
			while (a[j] > pivot) j--;
			if (i <= j) {
				std::swap(a[i], a[j]);
				i++; j--;
			}
		} while (i <= j);
	}
#pragma omp parallel num_threads(4) shared(a)
	{
		if (j > 0) sortArray(a, j);
		if (n > i) sortArray(a + i, n - i);
	}
	}
}
};
class Calculations{
public:
	virtual int Sum(int arr[][100], int n, int begRow, int endRow, int begCol, int endCol){
		int sum = 0;
#pragma omp parallel  shared(arr,n)  reduction(+:sum) 	
#pragma omp for 
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++){
			if ((j >= begRow) && (j <= endRow) && (i >= begCol) && (i <= endCol))
			{
				sum = sum + (int)arr[i][j];
			}
			}
		return sum;
	}

	virtual int SumByLetter(int array[][100], int n)
	{
		return Sum(array, n, 0, 30, 0, 90) + Sum(array, n, 31, 99, 0, 10) + Sum(array, n, 31, 99, 40, 50) + Sum(array, n, 31, 100, 90, 99);
	}
};


int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_CTYPE, "rus");
	const int n = 100;
	int array[n][n];  int a[n*n];
	MyArray myArray; Calculations calculations;
	myArray.InitArray(array, n);

	std::cout << "�������� ������:" << "\n";
	myArray.ShowArray(array);

	myArray.ToOneDimentionalArray(array, a, n);

	myArray.sortArray(a, n*n - 1);

	myArray.ToMultiDimentionalArray(array, a, n);

	std::cout << "��������������� ������:" << "\n";
	myArray.ShowArray(array);

	std::cout << "Sum of elements:" << calculations.SumByLetter(array, n) << "\n";
	system("PAUSE");
	return 0;
}

