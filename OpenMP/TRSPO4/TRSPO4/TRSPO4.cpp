// TRSPO4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <omp.h>
#include <iostream>
#include <Windows.h>
#include <time.h>

static class Drawing {
public:
	static void DrawRectangle(char* arr[][36], int n, int begRow, int endRow, int begCol, int endCol){
#pragma omp parallel   
#pragma omp for 
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++){
				if ((j >= begRow) && (j <= endRow) && (i >= begCol) && (i <= endCol))
					arr[i][j] ="*";
				}
		}

	static void ShowMatrix(char* arr[][36]){
#pragma omp parallel 
#pragma omp for ordered
		for (int i = 0; i < 36; i++)
		{
#pragma omp ordered
			for (int j = 0; j < 36; j++){
				std::cout << arr[i][j] << " ";
			}
			std::cout << "\n";
		}
	}
};

	int  _tmain(int argc, _TCHAR* argv[])
	{
		const int n = 36;
		/* �������� �������� ������� */
		char* A[n][n];
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++){
				A[i][j] = " ";
				}
				Drawing::DrawRectangle(A, n, 0, 10, 0, 36);
				Drawing::DrawRectangle(A, n, 8, 20, 14, 22);
				Drawing::DrawRectangle(A, n, 16, 20, 8, 28);
				Drawing::DrawRectangle(A, n, 20, 24, 4, 16);
				Drawing::DrawRectangle(A, n, 20, 24, 20, 32);
				Drawing::DrawRectangle(A, n, 24, 28, 0, 12);
				Drawing::DrawRectangle(A, n, 24, 28, 24, 36);
				Drawing::DrawRectangle(A, n, 28, 32, 0, 8);
				Drawing::DrawRectangle(A, n, 28, 32, 28, 36);	
    Drawing::ShowMatrix(A);
		system("PAUSE");
		return 0;
	}