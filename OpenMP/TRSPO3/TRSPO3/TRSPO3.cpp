// TRSPO3.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <omp.h>
#include <iostream>
#include <Windows.h>
#include <time.h>

static class MultiplyingResult {
public:
	static void SerialMultiplying(int* arr, int n){
		double serialCount = 1; int i = 0, j = 0;

		double sStart = omp_get_wtime();
		for (i = 0; i < n; i++)
			for (j = 0; j < n; j++){
			int l = n*i + j;
			serialCount = serialCount*arr[l];
			}
		std::cout << "Time of calculate " << omp_get_wtime() - sStart << " seconds" << std::endl;
		std::cout << "SerialResult " << serialCount << std::endl;
	}

	static void ParallelMultiplying(int *arr, int n)
	{
		double count = 1; int i, j = 0;
		double pStart = omp_get_wtime();

#pragma omp parallel shared(arr,n) 	private(i) 
		{

#pragma omp for 

			for (i = 0; i < n; i++)
				for (j = 0; j < n; j++){
				int l = n*i + j;
				count = count*arr[l];
				}

		}
		std::cout << "Time of calculate " << omp_get_wtime() - pStart << " seconds" << std::endl;
		std::cout << "ParallelResult " << count << std::endl;
	}
};
int main(int argc, char* argv[])
{
	const int n = 15;
	int A[n][n], i, j, count = 1, serialCount = 1;
	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++){
		A[i][j] = 7;
		}

	MultiplyingResult::SerialMultiplying((int*)A, n);
	MultiplyingResult::ParallelMultiplying((int*)A, n);
	system("PAUSE");
	return 0;
}

